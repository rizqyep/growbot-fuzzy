# Grow Bot Fuzzy

Grow Bot adalah sebuah sistem control behaviour robot untuk menyiram tanaman dengan menganalisa faktor-faktor seperti: Suhu Tanaman, Kelembaban Tanah, serta Tingkat Kehijauan Tanaman.

## Instalasi

Project Tugas Besar ini menggunakan Javascript dengan node sebagai environtment utamanya. Untuk menjalankan sistem ini dilakukan dengan instalasi NodeJS terlebih dahulu

### Install Homebrew (macOS)

```bash
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Setelah Homebrew selesai diinstall ke dalam sistem operasi atau perangkat, langkah berikutnya adalah meng-install NPM

### Install NPM dan NodeJS

Jalankan command berikut pada terminal untuk meng-install NodeJS dan NPM

```bash
brew install node
```

Setelah proses instalasi NodeJS selesai, langkah selanjutnya adalah masuk ke folder atau direktori project Grow Bot Fuzzy. Buatlah sebuah file dengan nama `.env` dan isikan dengan:

```txt
PORT = 3000
```

nomor port bisa diganti sesuai port yang terbuka pada device

### Install Dotenv

Untuk dapat menjalankan program Grow Bot Fuzzy, terlebih dahulu dengan meng-install Dotenv. Jalankan command dibawah pada direktori Grow Bot Fuzy:

```bash
npm install dotenv
```

## Menjalankan Program Grow Bot Fuzzy

Untuk menjalankan program Grow Bot Fuzzy, masuk ke direktori project Grow Bot Fuzzy dan jalankan command berikut:

```bash
npm run startapi
```

Setelah dirun dan mendapatkan output `Running!`, jangan tutup terminal. Sekarang buka browser dan kunjungi link berikut:

```txt
http://localhost:3000/dashboard
```
