    const getRandomInt = (max) => {
        return Math.floor(Math.random() * max);
    }
    let fuzzificationResult = document.getElementById('fuzzyficationResult');
    let inferenceContainer = document.getElementById('inferenceContainer');
    let minImplicationContainer = document.getElementById('minImplicationContainer');
    let maxImplicationContainer = document.getElementById('maxImplicationContainer');
    let mamdaniContainer = document.getElementById('mamdaniContainer');

    let temperatureImplication = [];
    let humidityImplication = [];
    let greenLevelImplication = [];


    window.addEventListener('load', () => {
        inferenceContainer.hidden = true;
        fuzzyficationResult.hidden = true;
        minImplicationContainer.hidden = true;
        maxImplicationContainer.hidden = true;
        mamdaniContainer.hidden = true;

    });

    let submitBtn = document.getElementById('submitBtn');
    submitBtn.addEventListener('click', (event) => {
        event.preventDefault();
        inferenceContainer.hidden = true;
        fuzzyficationResult.hidden = true;
        minImplicationContainer.hidden = true;
        maxImplicationContainer.hidden = true;
        mamdaniContainer.hidden = true;
        let temperatureValue = document.getElementById('temperatureInput').value;
        let humidityValue = document.getElementById('humidityInput').value;
        let greenLevelValue = document.getElementById('greenLevelInput').value;

        fetch('http://103.214.112.84:3104', {
                method: "POST",
                body: JSON.stringify({
                    temperature: temperatureValue,
                    humidity: humidityValue,
                    greenLevel: greenLevelValue
                }),
                headers: {
                    "Content-type": "application/json"
                }
            })
            .then(response => response.json())
            .then((response) => {
                temperatureFuzzyView(response.temperatureFuzzyMember);
                humidityFuzzyView(response.humidityFuzzyMember);
                greenLevelFuzzyView(response.greenLevelFuzzyMember);
                inferenceView(response.rulesApplied, response.rules);
                minImplicationView(response.minimumImplicationResult, response.rulesApplied, response.rules);
                maxImplicationView(response.maximumImplicationResult, response.minimumImplicationResult);
                mamdaniView(response.mamdaniDefuzzificationResult, response.maximumImplicationResult);
                setTimeout(() => {
                    fuzzyficationResult.hidden = false;
                }, 2000);

                setTimeout(() => {
                    inferenceContainer.hidden = false;
                }, 3000);
                setTimeout(() => {
                    minImplicationContainer.hidden = false;
                }, 4000);
                setTimeout(() => {
                    maxImplicationContainer.hidden = false;
                }, 5000);
                setTimeout(() => {
                    mamdaniContainer.hidden = false;
                }, 5000);
            })
            .catch(err => console.log(err));

    });

    const temperatureFuzzyView = (temperatureMember) => {
        let temperatureFuzzyResult = document.getElementById('temperatureFuzzyResult');

        temperatureFuzzyResult.innerHTML = `
                        <div class ="d-flex justify-content-around">
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold">${temperatureMember.Dingin.toFixed(3)}</h2>
                                <p>Dingin</p>
                            </div>
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold">${temperatureMember.Sejuk.toFixed(3)}</h2>
                                <p class ="font-weight-bold">Sejuk</p>
                            </div>
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold">${temperatureMember.Normal.toFixed(3)}</h2>
                                <p class ="font-weight-bold">Normal</p>
                            </div>
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold">${temperatureMember.Hangat.toFixed(3)}</h2>
                                <p class ="font-weight-bold">Hangat</p>
                            </div>
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold">${temperatureMember.Panas.toFixed(3)}</h2>
                                <p class ="font-weight-bold">Panas</p>
                            </div>

                        </div>
                    `;
    }

    const humidityFuzzyView = (humidityMember) => {
        let humidityFuzzyResult = document.getElementById('humidityFuzzyResult');

        humidityFuzzyResult.innerHTML = `
                        <div class ="d-flex justify-content-around">
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold">${humidityMember.Kering.toFixed(3)}</h2>
                                <p class ="font-weight-bold">Kering</p>
                            </div>
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold">${humidityMember.Lembab.toFixed(3)}</h2>
                                <p class ="font-weight-bold">Lembab</p>
                            </div>
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold">${humidityMember.Basah.toFixed(3)}</h2>
                                <p class ="font-weight-bold">Basah</p>
                            </div>
                      

                        </div>
                    `;
    }

    const greenLevelFuzzyView = (greenLevelMember) => {
        let greenLevelFuzzyResult = document.getElementById('greenLevelFuzzyResult');

        greenLevelFuzzyResult.innerHTML = `
                        <div class ="d-flex justify-content-around">
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold">${greenLevelMember.Rendah.toFixed(3)}</h2>
                                <p class ="font-weight-bold">Rendah</p>
                            </div>
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold">${greenLevelMember.Sedang.toFixed(3)}</h2>
                                <p class ="font-weight-bold" >Sedang</p>
                            </div>
                            <div class ="text-light text-center">
                                <h2 class ="font-weight-bold" >${greenLevelMember.Tinggi.toFixed(3)}</h2>
                                <p class ="font-weight-bold" >Tinggi</p>
                            </div>
                      

                        </div>
                    `;


    }



    const inferenceView = (inferenceResult, rules) => {
        const newRules = {};
        let inferenceResultView = document.getElementById('inferenceResult');
        let i = 1;
        inferenceResultView.innerHTML = '';
        rules.forEach((rule) => {

            newRules[i] = rule;
            i++;
        })
        console.log(newRules);
        inferenceResult.forEach((ruleApplied) => {
            let temperature = newRules[ruleApplied.ruleNumber].temperatureLinguisticValue;
            let humidity = newRules[ruleApplied.ruleNumber].humidityLinguisticValue;
            let greenLevel = newRules[ruleApplied.ruleNumber].greenLevelLinguisticValue;

            inferenceResultView.innerHTML += `
                            <p> ${ruleApplied.ruleNumber} . IF TEMPERATURE = <span class ="font-weight-bold"> 
                                ${ newRules[ruleApplied.ruleNumber].temperatureLinguisticValue} </span>
                                AND HUMIDITY = <span class ="font-weight-bold">
                                    ${newRules[ruleApplied.ruleNumber].humidityLinguisticValue}</span> 
                                AND GREEN_LEVEL = 
                                <span class="font-weight-bold">
                                    ${ newRules[ruleApplied.ruleNumber].greenLevelLinguisticValue} </span>
                                THEN SIRAM = <span class ="font-weight-bold">${ newRules[ruleApplied.ruleNumber].output} </span> </p>
                        <p>
                                Details :<br>
                                Temperature :  
                                ${ newRules[ruleApplied.ruleNumber].temperatureLinguisticValue}  (${ruleApplied.temperatureFuzzyMember.toFixed(3)})<br>
                                Humidity :    
                                ${newRules[ruleApplied.ruleNumber].humidityLinguisticValue}  (${ruleApplied.humidityFuzzyMember.toFixed(3)})<br> 
                                Green Level : 
                                ${ newRules[ruleApplied.ruleNumber].greenLevelLinguisticValue} (${ruleApplied.greenLevelFuzzyMember.toFixed(3)})


                        </p>`;

            temperatureImplication.push(ruleApplied.temperatureFuzzyMember);
            humidityImplication.push(ruleApplied.humidityFuzzyMember);
            greenLevelImplication.push(ruleApplied.greenLevelFuzzyMember);
        });
    }

    const minImplicationView = (result, rulesApplied, rules) => {
        const newRules = {};
        let i = 1;
        rules.forEach((rule) => {

            newRules[i] = rule;
            i++;
        })
        let minimumImplicationResult = document.getElementById('minImplicationResult');
        minimumImplicationResult.innerHTML = '';
        i = 0;
        let countTidak = 0;
        let countSiram = 0;

        let actionKey = {
            true: "Siram",
            false: "Tidak"
        }

        rulesApplied.forEach((ruleApplied) => {
            minimumImplicationResult.innerHTML += `
                         <p> ${ruleApplied.ruleNumber} . IF SUHU = <span class ="font-weight-bold"> 
                                ${ newRules[ruleApplied.ruleNumber].temperatureLinguisticValue} </span>
                                AND KELEMBABAN = <span class ="font-weight-bold">
                                    ${newRules[ruleApplied.ruleNumber].humidityLinguisticValue}</span> 
                                AND TINGKAT KEHIJAUAN = 
                                <span class="font-weight-bold">
                                    ${ newRules[ruleApplied.ruleNumber].greenLevelLinguisticValue} </span>
                                THEN SIRAM = <span class ="font-weight-bold">${ newRules[ruleApplied.ruleNumber].output} </span> </p>
                        <p>
                            <p>
                              
                                µ ${actionKey[newRules[ruleApplied.ruleNumber].output]} = µ${ newRules[ruleApplied.ruleNumber].temperatureLinguisticValue} 
                                ˄ µ${ newRules[ruleApplied.ruleNumber].humidityLinguisticValue} 
                                ˄ µ${ newRules[ruleApplied.ruleNumber].greenLevelLinguisticValue}<br>
                                = min(${ruleApplied.temperatureFuzzyMember.toFixed(3)} 
                                ;${ruleApplied.humidityFuzzyMember.toFixed(3)} ; ${ruleApplied.greenLevelFuzzyMember.toFixed(3)})
                            </p>
                        `;
            if (newRules[ruleApplied.ruleNumber].output == true) {
                minimumImplicationResult.innerHTML += `${result.Siram[countSiram].toFixed(3)}<hr>`;
                countSiram += 1;
            }

            if (newRules[ruleApplied.ruleNumber].output == false) {
                minimumImplicationResult.innerHTML += `${result.Tidak[countTidak].toFixed(3)}<hr>`;
                countTidak += 1;
            }
        })
    }

    let maxImplicationView = (maxResult, minResult) => {
        let maxImplicationResult = document.getElementById('maxImplicationResult');
        maxImplicationResult.innerHTML = '';
        if (minResult.Tidak.length > 0) {
            maxImplicationResult.innerHTML += `<span>µTidak = </span>`;
            for (let i = 0; i < minResult.Tidak.length; i++) {
                maxImplicationResult.innerHTML += ` <span>µTidak[${minResult.Tidak[i].toFixed(3)}] </span> `;
                if (i != (minResult.Tidak.length - 1)) {
                    maxImplicationResult.innerHTML += "<span>˅</span>";
                }
            }
            maxImplicationResult.innerHTML += `<p>${maxResult.Tidak.toFixed(3)}</p>`;

        }

        if (minResult.Siram.length > 0) {
            maxImplicationResult.innerHTML += ` <span>µSiram = </span>`;
            for (let i = 0; i < minResult.Siram.length; i++) {
                maxImplicationResult.innerHTML += ` <span>µSiram[${minResult.Siram[i].toFixed(3)}] </span> `;
                if (i != minResult.Siram.length - 1) {
                    maxImplicationResult.innerHTML += "<span>˅</span>";
                }
            }
            maxImplicationResult.innerHTML += `<p>${maxResult.Siram.toFixed(3)}</p>`;
        }
    }

    let mamdaniView = (result, maxResult) => {
        let mamdaniResult = document.getElementById('mamdaniResult');
        mamdaniResult.innerHTML = '';
        let tidakSample = [10, 20, 30, 40, 50];
        let siramSample = [65, 70, 80, 90, 100];


        let upperEquation = `((10 + 20 + 30 + 40 + 50) x ${maxResult.Tidak.toFixed(3)}) + (65+70+80+90+100) x ${maxResult.Siram.toFixed(3)}`;

        let upperEquationResult = (((10 + 20 + 30 + 40 + 50) * maxResult.Tidak) + ((65 + 70 + 80 + 90 + 100) * maxResult.Siram)).toFixed(3);
        let bottomEquationResult = (maxResult.Tidak * 5) + (maxResult.Siram * 5);

        let bottomEquation = "(";
        for (let i = 0; i < 5; i++) {
            bottomEquation += `${maxResult.Tidak.toFixed(3)}`;
            if (i != 4) {
                bottomEquation += "+"
            }
        }
        bottomEquation += ") + (";
        for (let i = 0; i < 5; i++) {
            bottomEquation += `${maxResult.Siram.toFixed(3)}`;
            if (i != 4) {
                bottomEquation += "+"
            }
        }
        bottomEquation += ")";



        mamdaniResult.innerHTML += `
                    <div class ="d-flex justify-content-start align-items-center">
                        <div class ="left mr-3 "> y' = </div>
                        <div class ="mid mr-3 text-center">
                            ${upperEquation}
                            <hr class ="elegant-color text-dark">
                            ${bottomEquation}
                        </div>
                        <div class ="equal mr-3 text-center">
                            =
                        </div>
                        <div class ="right text-center">
                            ${upperEquationResult}
                            <hr class ="elegant-color text-dark">
                            ${bottomEquationResult}
                        </div>
                    </div>`;

        mamdaniResult.innerHTML += `<p class ="font-weight-bold mt-3">y' = ${result.toFixed(3)}</p>`;

    }