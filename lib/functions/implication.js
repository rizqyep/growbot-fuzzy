const Rules = require("../constants/numberedRules.js");
let rules = Rules.RULES;

const minimumImplication = (rulesApplied) => {
    let minimumImplicationResult = {
        Tidak: [],
        Siram: []
    };
    rulesApplied.forEach((ruleApplied) => {

        if (ruleApplied.temperatureFuzzyMember <= ruleApplied.humidityFuzzyMember && ruleApplied.temperatureFuzzyMember <= ruleApplied.greenLevelFuzzyMember) {
            if (rules[String(ruleApplied.ruleNumber)].output == false) {

                minimumImplicationResult.Tidak.push(ruleApplied.temperatureFuzzyMember);
            } else {

                minimumImplicationResult.Siram.push(ruleApplied.temperatureFuzzyMember);
            }

        } else if (ruleApplied.humidityFuzzyMember <= ruleApplied.temperatureFuzzyMember && ruleApplied.humidityFuzzyMember <= ruleApplied.greenLevelFuzzyMember) {
            if (rules[String(ruleApplied.ruleNumber)].output == false) {

                minimumImplicationResult.Tidak.push(ruleApplied.humidityFuzzyMember);
            } else {

                minimumImplicationResult.Siram.push(ruleApplied.humidityFuzzyMember);
            }
        } else {
            if (rules[String(ruleApplied.ruleNumber)].output == false) {

                minimumImplicationResult.Tidak.push(ruleApplied.greenLevelFuzzyMember);
            } else {

                minimumImplicationResult.Siram.push(ruleApplied.greenLevelFuzzyMember);
            }
        }
    });


    return minimumImplicationResult;
}


const maximumImplication = (minimumImplicationResult) => {
    let maximumImplicationResult = {
        Tidak: 0,
        Siram: 0
    };
    let currentTidakMax = minimumImplicationResult.Tidak[0];
    if (minimumImplicationResult.Tidak.length > 0) {
        for (let i = 1; i < minimumImplicationResult.Tidak.length; i++) {
            if (minimumImplicationResult.Tidak[i] > currentTidakMax) {
                currentTidakMax = minimumImplicationResult.Tidak[i];
            }
        }
        maximumImplicationResult.Tidak = currentTidakMax;

    }
    let currentSiramMax = minimumImplicationResult.Siram[0];
    if (minimumImplicationResult.Siram.length > 0) {
        for (let i = 1; i < minimumImplicationResult.Siram.length; i++) {
            if (minimumImplicationResult.Siram[i] > currentSiramMax) {
                currentSiramMax = minimumImplicationResult.Siram[i];
            }
        }
        maximumImplicationResult.Siram = currentSiramMax;
    }
    return maximumImplicationResult;
}
module.exports = { minimumImplication, maximumImplication }