const Rules = require("../constants/rules");
let rules = Rules.RULES;
const displayRules = () => {

    let i = 1;
    rules.forEach((rule) => {

        console.log(`${i} IF TEMPERATURE = ${rule.temperatureLinguisticValue} AND HUMIDITY = ${rule.humidityLinguisticValue} AND GREEN_LEVEL = ${rule.greenLevelLinguisticValue} THEN SIRAM = ${rule.output}`)
        i++;
    })
}

const inferenceEngine = (temperatureFuzzyMember, humidityFuzzyMember, greenLevelFuzzyMember) => {
    let i = 1;
    let rulesApplied = [];
    rules.forEach((rule) => {
        if (temperatureFuzzyMember[rule.temperatureLinguisticValue] != 0 &&
            humidityFuzzyMember[rule.humidityLinguisticValue] != 0 &&
            greenLevelFuzzyMember[rule.greenLevelLinguisticValue] != 0
        ) {
            rulesApplied.push({
                ruleNumber: i,
                temperatureFuzzyMember: temperatureFuzzyMember[rule.temperatureLinguisticValue],
                humidityFuzzyMember: humidityFuzzyMember[rule.humidityLinguisticValue],
                greenLevelFuzzyMember: greenLevelFuzzyMember[rule.greenLevelLinguisticValue],
            });
        }
        i++;
    })
    return rulesApplied;
}


const rulesList = () => {
    return rules;
}
module.exports = { displayRules, inferenceEngine, rulesList };