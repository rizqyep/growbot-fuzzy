let Linguistic = require("../constants/linguistic");

/**
 * HUMIDITY LINGUSTIC
 */
const humidityLinguistic = (humidity) => {
  let humidityFuzzyMember = {
    Kering: 0,
    Lembab: 0,
    Basah: 0,
  };

  /**
   * For "Kering" member
   */

  // Absolute State
  if (humidity <= Linguistic.HUMIDITY["Kering"].upperBound) {
    humidityFuzzyMember["Kering"] = 1;
  }

  // Declining State
  else if (
    humidity > Linguistic.HUMIDITY["Kering"].upperBound &&
    humidity < Linguistic.HUMIDITY["Lembab"].lowerBound
  ) {
    humidityFuzzyMember["Kering"] =
      (Linguistic.HUMIDITY["Lembab"].lowerBound - humidity) /
      (Linguistic.HUMIDITY["Lembab"].lowerBound -
        Linguistic.HUMIDITY["Kering"].upperBound);
  }

  /**
   * For "Lembab" member
   */

  // Climbing State
  if (
    humidity > Linguistic.HUMIDITY["Kering"].upperBound &&
    humidity < Linguistic.HUMIDITY["Lembab"].lowerBound
  ) {
    humidityFuzzyMember["Lembab"] =
      (humidity - Linguistic.HUMIDITY["Kering"].upperBound) /
      (Linguistic.HUMIDITY["Lembab"].lowerBound -
        Linguistic.HUMIDITY["Kering"].upperBound);
  }

  // Absolute State
  else if (
    humidity >= Linguistic.HUMIDITY["Lembab"].lowerBound &&
    humidity <= Linguistic.HUMIDITY["Lembab"].upperBound
  ) {
    humidityFuzzyMember["Lembab"] = 1;
  }

  // Declining State
  else if (
    humidity > Linguistic.HUMIDITY["Lembab"].upperBound &&
    humidity < Linguistic.HUMIDITY["Basah"].lowerBound
  ) {
    humidityFuzzyMember["Lembab"] =
      (Linguistic.HUMIDITY["Basah"].lowerBound - humidity) /
      (Linguistic.HUMIDITY["Basah"].lowerBound -
        Linguistic.HUMIDITY["Lembab"].upperBound);
  }

  /**
   * For "Basah" member
   */

  // Climbing State
  if (
    humidity > Linguistic.HUMIDITY["Lembab"].upperBound &&
    humidity < Linguistic.HUMIDITY["Basah"].lowerBound
  ) {
    humidityFuzzyMember["Basah"] =
      (humidity - Linguistic.HUMIDITY["Lembab"].upperBound) /
      (Linguistic.HUMIDITY["Basah"].lowerBound -
        Linguistic.HUMIDITY["Lembab"].upperBound);
  }

  // Absolute State
  else if (humidity >= Linguistic.HUMIDITY["Basah"].lowerBound) {
    humidityFuzzyMember["Basah"] = 1;
  }

  return humidityFuzzyMember;
};

module.exports = humidityLinguistic;
