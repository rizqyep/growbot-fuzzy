let Linguistic = require("../constants/linguistic");

const temperatureLinguistic = (temperature) => {
    let temperatureFuzzyMember = {
        Dingin: 0,
        Sejuk: 0,
        Hangat: 0,
        Panas: 0,
        Normal: 0
    };

    if (temperature <= Linguistic.TEMPERATURE["Dingin"].upperBound) {
        temperatureFuzzyMember["Dingin"] = 1;
    } else if (
        temperature > Linguistic.TEMPERATURE["Dingin"].upperBound &&
        temperature < Linguistic.TEMPERATURE["Sejuk"].lowerBound
    ) {
        temperatureFuzzyMember["Dingin"] =
            (Linguistic.TEMPERATURE["Sejuk"].lowerBound - temperature) /
            (Linguistic.TEMPERATURE["Sejuk"].lowerBound -
                Linguistic.TEMPERATURE["Dingin"].upperBound);
    }

    //Cek temperature Dingin Member

    //Climbing State
    if (
        temperature > Linguistic.TEMPERATURE["Dingin"].upperBound &&
        temperature < Linguistic.TEMPERATURE["Sejuk"].lowerBound
    ) {
        temperatureFuzzyMember["Sejuk"] =
            (temperature - Linguistic.TEMPERATURE["Dingin"].upperBound) /
            (Linguistic.TEMPERATURE["Sejuk"].lowerBound -
                Linguistic.TEMPERATURE["Dingin"].upperBound);
    }

    //Absolute State
    else if (
        temperature >= Linguistic.TEMPERATURE["Sejuk"].lowerBound &&
        temperature <= Linguistic.TEMPERATURE["Sejuk"].upperBound
    ) {
        temperatureFuzzyMember["Sejuk"] = 1;
    }

    //Declining State
    else if (
        temperature > Linguistic.TEMPERATURE["Sejuk"].upperBound &&
        temperature < Linguistic.TEMPERATURE["Normal"].lowerBound
    ) {
        temperatureFuzzyMember["Sejuk"] =
            (Linguistic.TEMPERATURE["Normal"].lowerBound - temperature) /
            (Linguistic.TEMPERATURE["Normal"].lowerBound -
                Linguistic.TEMPERATURE["Sejuk"].upperBound);
    }

    //Cek temperature Normal Member

    //Climbing State

    if (
        temperature > Linguistic.TEMPERATURE["Sejuk"].upperBound &&
        temperature < Linguistic.TEMPERATURE["Normal"].lowerBound
    ) {
        temperatureFuzzyMember["Normal"] =
            (temperature - Linguistic.TEMPERATURE["Sejuk"].upperBound) /
            (Linguistic.TEMPERATURE["Normal"].lowerBound -
                Linguistic.TEMPERATURE["Sejuk"].upperBound);
    }

    //Absolute State
    else if (
        temperature >= Linguistic.TEMPERATURE["Normal"].lowerBound &&
        temperature <= Linguistic.TEMPERATURE["Normal"].upperBound
    ) {
        temperatureFuzzyMember["Normal"] = 1;
    }

    //Declining State
    else if (
        temperature > Linguistic.TEMPERATURE["Normal"].upperBound &&
        temperature < Linguistic.TEMPERATURE["Hangat"].lowerBound
    ) {
        temperatureFuzzyMember["Normal"] =
            (Linguistic.TEMPERATURE["Hangat"].lowerBound - temperature) /
            (Linguistic.TEMPERATURE["Hangat"].lowerBound -
                Linguistic.TEMPERATURE["Normal"].upperBound);
    }

    //Cek temperature Hangat Member

    //Climbing State

    if (
        temperature > Linguistic.TEMPERATURE["Normal"].upperBound &&
        temperature < Linguistic.TEMPERATURE["Hangat"].lowerBound
    ) {
        temperatureFuzzyMember["Hangat"] =
            (temperature - Linguistic.TEMPERATURE["Normal"].upperBound) /
            (Linguistic.TEMPERATURE["Hangat"].lowerBound -
                Linguistic.TEMPERATURE["Normal"].upperBound);
    }

    //Absolute State
    else if (
        temperature >= Linguistic.TEMPERATURE["Hangat"].lowerBound &&
        temperature <= Linguistic.TEMPERATURE["Hangat"].upperBound
    ) {
        temperatureFuzzyMember["Hangat"] = 1;
    }

    //Declining State
    else if (
        temperature > Linguistic.TEMPERATURE["Hangat"].upperBound &&
        temperature < Linguistic.TEMPERATURE["Panas"].lowerBound
    ) {
        temperatureFuzzyMember["Hangat"] =
            (Linguistic.TEMPERATURE["Panas"].lowerBound - temperature) /
            (Linguistic.TEMPERATURE["Panas"].lowerBound -
                Linguistic.TEMPERATURE["Hangat"].upperBound);
    }

    //Cek temperature Panas Member

    //Climbing State
    if (
        temperature > Linguistic.TEMPERATURE["Hangat"].upperBound &&
        temperature < Linguistic.TEMPERATURE["Panas"].lowerBound
    ) {
        temperatureFuzzyMember["Panas"] =
            (temperature - Linguistic.TEMPERATURE["Hangat"].upperBound) /
            (Linguistic.TEMPERATURE["Panas"].lowerBound -
                Linguistic.TEMPERATURE["Hangat"].upperBound);
    }

    // Absolute State
    else if (temperature >= Linguistic.TEMPERATURE["Panas"].lowerBound) {
        temperatureFuzzyMember["Panas"] = 1;
    }

    return temperatureFuzzyMember;
};

module.exports = temperatureLinguistic;