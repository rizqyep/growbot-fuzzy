let Linguistic = require("../constants/linguistic");

const greenLevelLinguistic = (color) => {
  let greenLevelFuzzyMember = {
    Rendah: 0,
    Sedang: 0,
    Tinggi: 0,
  };

  /**
   * For "Rendah" member
   */

  // Absolute State
  if (color <= Linguistic.GREEN_LEVEL["Rendah"].upperBound) {
    greenLevelFuzzyMember["Rendah"] = 1;
  }

  // Declining State
  else if (
    color > Linguistic.GREEN_LEVEL["Rendah"].upperBound &&
    color < Linguistic.GREEN_LEVEL["Sedang"].lowerBound
  ) {
    greenLevelFuzzyMember["Rendah"] =
      (Linguistic.GREEN_LEVEL["Sedang"].lowerBound - color) /
      (Linguistic.GREEN_LEVEL["Sedang"].lowerBound -
        Linguistic.GREEN_LEVEL["Rendah"].upperBound);
  }

  /**
   * For "Sedang" member
   */

  // Climbing State
  if (
    color > Linguistic.GREEN_LEVEL["Rendah"].upperBound &&
    color < Linguistic.GREEN_LEVEL["Sedang"].lowerBound
  ) {
    greenLevelFuzzyMember["Sedang"] =
      (color - Linguistic.GREEN_LEVEL["Rendah"].upperBound) /
      (Linguistic.GREEN_LEVEL["Sedang"].lowerBound -
        Linguistic.GREEN_LEVEL["Rendah"].upperBound);
  }

  // Absolute State
  else if (
    color >= Linguistic.GREEN_LEVEL["Sedang"].lowerBound &&
    color <= Linguistic.GREEN_LEVEL["Sedang"].upperBound
  ) {
    greenLevelFuzzyMember["Sedang"] = 1;
  }

  // Declining State
  else if (
    color > Linguistic.GREEN_LEVEL["Sedang"].upperBound &&
    color < Linguistic.GREEN_LEVEL["Tinggi"].lowerBound
  ) {
    greenLevelFuzzyMember["Sedang"] =
      (Linguistic.GREEN_LEVEL["Tinggi"].lowerBound - color) /
      (Linguistic.GREEN_LEVEL["Tinggi"].lowerBound -
        Linguistic.GREEN_LEVEL["Sedang"].upperBound);
  }

  /**
   * For "Tinggi" member
   */

  // Climbing State
  if (
    color > Linguistic.GREEN_LEVEL["Sedang"].upperBound &&
    color < Linguistic.GREEN_LEVEL["Tinggi"].lowerBound
  ) {
    greenLevelFuzzyMember["Tinggi"] =
      (color - Linguistic.GREEN_LEVEL["Sedang"].upperBound) /
      (Linguistic.GREEN_LEVEL["Tinggi"].lowerBound -
        Linguistic.GREEN_LEVEL["Sedang"].upperBound);
  }

  // Absolute State
  else if (color >= Linguistic.GREEN_LEVEL["Tinggi"].lowerBound) {
    greenLevelFuzzyMember["Tinggi"] = 1;
  }

  return greenLevelFuzzyMember;
};

module.exports = greenLevelLinguistic;
