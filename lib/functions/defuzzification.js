const mamdaniDefuzzification = (maximumImplicationResult) => {

    let tidakSamplingSum = 0;
    let siramSamplingSum = 0;

    for (let i = 10; i <= 50; i += 10) {

        tidakSamplingSum += i;
    }

    for (let i = 60; i <= 100; i += 10) {
        if (i == 60) {

            siramSamplingSum += 65;
        } else {

            siramSamplingSum += i;
        }
    }


    let result = tidakSamplingSum * maximumImplicationResult["Tidak"] + siramSamplingSum * maximumImplicationResult["Siram"];

    result = result / ((maximumImplicationResult["Tidak"] * 5) + (maximumImplicationResult["Siram"] * 5));
    return result;
}

module.exports = mamdaniDefuzzification;