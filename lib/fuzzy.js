let Linguistic = require("./constants/linguistic");
const Rules = require("./constants/rules");

let temperatureLinguistic = require("./functions/temperatureLinguistic");
let humidityLinguistic = require("./functions/humidityLinguistic");
let greenLevelLinguistic = require("./functions/greenLevelLinguistic");
let { inferenceEngine, displayRules, rulesList } = require("./functions/inferenceEngine");
let { minimumImplication, maximumImplication } = require("./functions/implication");
let mamdaniDefuzzification = require("./functions/defuzzification");


// console.log("RULES LIST : ");
// displayRules();

// // LOG
// let temperatureFuzzyMember = temperatureLinguistic(4)
// let humidityFuzzyMember = humidityLinguistic(23);
// let greenLevelFuzzyMember = greenLevelLinguistic(7);


// let rulesApplied = inferenceEngine(temperatureFuzzyMember, humidityFuzzyMember, greenLevelFuzzyMember);

// console.log("Rules Applied : ", rulesApplied);


// let minimumImplicationResult = minimumImplication(rulesApplied);
// console.log("Minimum Implication Result : ", minimumImplicationResult);

// let maximumImplicationResult = maximumImplication(minimumImplicationResult)

// console.log("Max Implication Result : ", maximumImplicationResult);

// let mamdaniDefuzzificationResult = mamdaniDefuzzification(maximumImplicationResult);

// console.log("Mamdani Defuzzification Result : ", mamdaniDefuzzificationResult);


const checkData = (request) => {

    // LOG
    let temperatureFuzzyMember = temperatureLinguistic(request.temperature);
    let humidityFuzzyMember = humidityLinguistic(request.humidity);
    let greenLevelFuzzyMember = greenLevelLinguistic(request.greenLevel);

    let rules = rulesList();
    let rulesApplied = inferenceEngine(temperatureFuzzyMember, humidityFuzzyMember, greenLevelFuzzyMember);



    let minimumImplicationResult = minimumImplication(rulesApplied);

    let maximumImplicationResult = maximumImplication(minimumImplicationResult)


    let mamdaniDefuzzificationResult = mamdaniDefuzzification(maximumImplicationResult);


    return {
        temperatureFuzzyMember,
        humidityFuzzyMember,
        greenLevelFuzzyMember,
        rules,
        rulesApplied,
        minimumImplicationResult,
        maximumImplicationResult,
        mamdaniDefuzzificationResult
    }

}

module.exports = { checkData };