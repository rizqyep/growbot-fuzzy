class Rules {
    static RULES = [{
            //Rules For Temp = Panas
            temperatureLinguisticValue: "Panas",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Rendah",
            output: true
        },
        {
            temperatureLinguisticValue: "Panas",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        {
            temperatureLinguisticValue: "Panas",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },
        {
            temperatureLinguisticValue: "Panas",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Rendah",
            output: true
        },
        {
            temperatureLinguisticValue: "Panas",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        {
            temperatureLinguisticValue: "Panas",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },
        {
            temperatureLinguisticValue: "Panas",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Rendah",
            output: true
        },
        {
            temperatureLinguisticValue: "Panas",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Sedang",
            output: true
        },

        {
            temperatureLinguisticValue: "Panas",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Tinggi",
            output: true
        },
        //Temp = Hangat
        {
            temperatureLinguisticValue: "Hangat",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Rendah",
            output: true
        },
        {
            temperatureLinguisticValue: "Hangat",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Sedang",
            output: true
        },
        {
            temperatureLinguisticValue: "Hangat",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },

        {
            temperatureLinguisticValue: "Hangat",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Rendah",
            output: true
        },
        {
            temperatureLinguisticValue: "Hangat",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Sedang",
            output: true
        },
        {
            temperatureLinguisticValue: "Hangat",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },

        {
            temperatureLinguisticValue: "Hangat",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Rendah",
            output: true
        },
        {
            temperatureLinguisticValue: "Hangat",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        {
            temperatureLinguisticValue: "Hangat",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },

        //Rules for TEMP = Normal
        {
            temperatureLinguisticValue: "Normal",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Rendah",
            output: true
        },
        {
            temperatureLinguisticValue: "Normal",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        {
            temperatureLinguisticValue: "Normal",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },

        {
            temperatureLinguisticValue: "Normal",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Rendah",
            output: true
        },
        {
            temperatureLinguisticValue: "Normal",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        {
            temperatureLinguisticValue: "Normal",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },

        {
            temperatureLinguisticValue: "Normal",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Rendah",
            output: false
        },
        {
            temperatureLinguisticValue: "Normal",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },
        {
            temperatureLinguisticValue: "Normal",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        //Rules for TEMP Suhu = Sejuk
        {
            temperatureLinguisticValue: "Sejuk",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Rendah",
            output: true
        },
        {
            temperatureLinguisticValue: "Sejuk",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Sedang",
            output: true
        },
        {
            temperatureLinguisticValue: "Sejuk",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },

        {
            temperatureLinguisticValue: "Sejuk",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Rendah",
            output: false
        },
        {
            temperatureLinguisticValue: "Sejuk",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        {
            temperatureLinguisticValue: "Sejuk",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },

        {
            temperatureLinguisticValue: "Sejuk",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Rendah",
            output: false
        },
        {
            temperatureLinguisticValue: "Sejuk",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        {
            temperatureLinguisticValue: "Sejuk",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },

        //Rules For Temp = Dingin
        {
            temperatureLinguisticValue: "Dingin",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Rendah",
            output: true
        },
        {
            temperatureLinguisticValue: "Dingin",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        {
            temperatureLinguisticValue: "Dingin",
            humidityLinguisticValue: "Kering",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },

        {
            temperatureLinguisticValue: "Dingin",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Rendah",
            output: false
        },
        {
            temperatureLinguisticValue: "Dingin",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        {
            temperatureLinguisticValue: "Dingin",
            humidityLinguisticValue: "Lembab",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },

        {
            temperatureLinguisticValue: "Dingin",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Rendah",
            output: false
        },
        {
            temperatureLinguisticValue: "Dingin",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Sedang",
            output: false
        },
        {
            temperatureLinguisticValue: "Dingin",
            humidityLinguisticValue: "Basah",
            greenLevelLinguisticValue: "Tinggi",
            output: false
        },
    ]
}


module.exports = Rules;