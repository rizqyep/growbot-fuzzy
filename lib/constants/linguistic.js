class Linguistic {
  static TEMPERATURE = {
    Dingin: {
      upperBound: 3,
    },
    Sejuk: {
      lowerBound: 5,
      upperBound: 16,
    },
    Normal: {
      lowerBound: 18,
      upperBound: 27,
    },
    Hangat: {
      lowerBound: 30,
      upperBound: 36,
    },
    Panas: {
      lowerBound: 39,
    },
  };

  static HUMIDITY = {
    Kering: {
      upperBound: 12,
    },
    Lembab: {
      lowerBound: 25,
      upperBound: 45,
    },
    Basah: {
      lowerBound: 49,
    },
  };

  static GREEN_LEVEL = {
    Rendah: {
      upperBound: 3,
    },
    Sedang: {
      lowerBound: 5,
      upperBound: 7,
    },
    Tinggi: {
      lowerBound: 9,
    },
  };
}

module.exports = Linguistic;
