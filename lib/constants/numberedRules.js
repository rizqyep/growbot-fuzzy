class Rules {
    static RULES = {
        '1': {
            temperatureLinguisticValue: 'Panas',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Rendah',
            output: true
        },
        '2': {
            temperatureLinguisticValue: 'Panas',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '3': {
            temperatureLinguisticValue: 'Panas',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '4': {
            temperatureLinguisticValue: 'Panas',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Rendah',
            output: true
        },
        '5': {
            temperatureLinguisticValue: 'Panas',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '6': {
            temperatureLinguisticValue: 'Panas',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '7': {
            temperatureLinguisticValue: 'Panas',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Rendah',
            output: true
        },
        '8': {
            temperatureLinguisticValue: 'Panas',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Sedang',
            output: true
        },
        '9': {
            temperatureLinguisticValue: 'Panas',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Tinggi',
            output: true
        },
        '10': {
            temperatureLinguisticValue: 'Hangat',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Rendah',
            output: true
        },
        '11': {
            temperatureLinguisticValue: 'Hangat',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Sedang',
            output: true
        },
        '12': {
            temperatureLinguisticValue: 'Hangat',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '13': {
            temperatureLinguisticValue: 'Hangat',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Rendah',
            output: true
        },
        '14': {
            temperatureLinguisticValue: 'Hangat',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Sedang',
            output: true
        },
        '15': {
            temperatureLinguisticValue: 'Hangat',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '16': {
            temperatureLinguisticValue: 'Hangat',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Rendah',
            output: true
        },
        '17': {
            temperatureLinguisticValue: 'Hangat',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '18': {
            temperatureLinguisticValue: 'Hangat',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '19': {
            temperatureLinguisticValue: 'Normal',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Rendah',
            output: true
        },
        '20': {
            temperatureLinguisticValue: 'Normal',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '21': {
            temperatureLinguisticValue: 'Normal',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '22': {
            temperatureLinguisticValue: 'Normal',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Rendah',
            output: true
        },
        '23': {
            temperatureLinguisticValue: 'Normal',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '24': {
            temperatureLinguisticValue: 'Normal',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '25': {
            temperatureLinguisticValue: 'Normal',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Rendah',
            output: false
        },
        '26': {
            temperatureLinguisticValue: 'Normal',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '27': {
            temperatureLinguisticValue: 'Normal',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '28': {
            temperatureLinguisticValue: 'Sejuk',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Rendah',
            output: true
        },
        '29': {
            temperatureLinguisticValue: 'Sejuk',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Sedang',
            output: true
        },
        '30': {
            temperatureLinguisticValue: 'Sejuk',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '31': {
            temperatureLinguisticValue: 'Sejuk',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Rendah',
            output: false
        },
        '32': {
            temperatureLinguisticValue: 'Sejuk',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '33': {
            temperatureLinguisticValue: 'Sejuk',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '34': {
            temperatureLinguisticValue: 'Sejuk',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Rendah',
            output: false
        },
        '35': {
            temperatureLinguisticValue: 'Sejuk',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '36': {
            temperatureLinguisticValue: 'Sejuk',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '37': {
            temperatureLinguisticValue: 'Dingin',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Rendah',
            output: true
        },
        '38': {
            temperatureLinguisticValue: 'Dingin',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '39': {
            temperatureLinguisticValue: 'Dingin',
            humidityLinguisticValue: 'Kering',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '40': {
            temperatureLinguisticValue: 'Dingin',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Rendah',
            output: false
        },
        '41': {
            temperatureLinguisticValue: 'Dingin',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '42': {
            temperatureLinguisticValue: 'Dingin',
            humidityLinguisticValue: 'Lembab',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        },
        '43': {
            temperatureLinguisticValue: 'Dingin',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Rendah',
            output: false
        },
        '44': {
            temperatureLinguisticValue: 'Dingin',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Sedang',
            output: false
        },
        '45': {
            temperatureLinguisticValue: 'Dingin',
            humidityLinguisticValue: 'Basah',
            greenLevelLinguisticValue: 'Tinggi',
            output: false
        }
    }
}
module.exports = Rules;