const express = require("express");
const cors = require("cors");
const app = express();
const { checkData } = require("./lib/fuzzy");
app.use(cors());
app.use(express.json());
app.set("views", "./views");
app.set("view engine", "ejs");
app.use(express.static("public"));
require("dotenv").config();

app.post("/", (req, res) => {
  return res.json(checkData(req.body));
});

app.get("/dashboard", (req, res) => {
  return res.render("index");
});

app.get("/custominput", (req, res) => {
  return res.render("custominput");
});

const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log("Running!");
});
